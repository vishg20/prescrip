# == Schema Information
#
# Table name: prescription_details
#
#  id                     :bigint(8)        not null, primary key
#  prescription_detail_id :integer
#  prescription_id        :integer
#  drug_name              :string(255)
#  morning                :boolean
#  quantity               :string(255)
#  afternoon              :boolean
#  night                  :boolean
#  before_food            :boolean
#  after_food             :boolean
#  comment                :text(65535)
#  tenant_id              :integer
#  prescription_master_id :bigint(8)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

require 'test_helper'

class PrescriptionDetailTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
