require "application_system_test_case"

class PrescriptionDetailsTest < ApplicationSystemTestCase
  setup do
    @prescription_detail = prescription_details(:one)
  end

  test "visiting the index" do
    visit prescription_details_url
    assert_selector "h1", text: "Prescription Details"
  end

  test "creating a Prescription detail" do
    visit prescription_details_url
    click_on "New Prescription Detail"

    fill_in "After food", with: @prescription_detail.after_food
    fill_in "Afternoon", with: @prescription_detail.afternoon
    fill_in "Before food", with: @prescription_detail.before_food
    fill_in "Comment", with: @prescription_detail.comment
    fill_in "Drug name", with: @prescription_detail.drug_name
    fill_in "Morning", with: @prescription_detail.morning
    fill_in "Night", with: @prescription_detail.night
    fill_in "Prescription detail", with: @prescription_detail.prescription_detail_id
    fill_in "Prescription", with: @prescription_detail.prescription_id
    fill_in "Prescription master", with: @prescription_detail.prescription_master_id
    fill_in "Quantity", with: @prescription_detail.quantity
    fill_in "Tenant", with: @prescription_detail.tenant_id
    click_on "Create Prescription detail"

    assert_text "Prescription detail was successfully created"
    click_on "Back"
  end

  test "updating a Prescription detail" do
    visit prescription_details_url
    click_on "Edit", match: :first

    fill_in "After food", with: @prescription_detail.after_food
    fill_in "Afternoon", with: @prescription_detail.afternoon
    fill_in "Before food", with: @prescription_detail.before_food
    fill_in "Comment", with: @prescription_detail.comment
    fill_in "Drug name", with: @prescription_detail.drug_name
    fill_in "Morning", with: @prescription_detail.morning
    fill_in "Night", with: @prescription_detail.night
    fill_in "Prescription detail", with: @prescription_detail.prescription_detail_id
    fill_in "Prescription", with: @prescription_detail.prescription_id
    fill_in "Prescription master", with: @prescription_detail.prescription_master_id
    fill_in "Quantity", with: @prescription_detail.quantity
    fill_in "Tenant", with: @prescription_detail.tenant_id
    click_on "Update Prescription detail"

    assert_text "Prescription detail was successfully updated"
    click_on "Back"
  end

  test "destroying a Prescription detail" do
    visit prescription_details_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Prescription detail was successfully destroyed"
  end
end
