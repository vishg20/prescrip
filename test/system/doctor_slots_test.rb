require "application_system_test_case"

class DoctorSlotsTest < ApplicationSystemTestCase
  setup do
    @doctor_slot = doctor_slots(:one)
  end

  test "visiting the index" do
    visit doctor_slots_url
    assert_selector "h1", text: "Doctor Slots"
  end

  test "creating a Doctor slot" do
    visit doctor_slots_url
    click_on "New Doctor Slot"

    fill_in "Affiliation", with: @doctor_slot.affiliation_id
    fill_in "Day", with: @doctor_slot.day_id
    fill_in "Doctor slot", with: @doctor_slot.doctor_slot_id
    fill_in "Slot", with: @doctor_slot.slot_id
    fill_in "Tenant", with: @doctor_slot.tenant_id
    click_on "Create Doctor slot"

    assert_text "Doctor slot was successfully created"
    click_on "Back"
  end

  test "updating a Doctor slot" do
    visit doctor_slots_url
    click_on "Edit", match: :first

    fill_in "Affiliation", with: @doctor_slot.affiliation_id
    fill_in "Day", with: @doctor_slot.day_id
    fill_in "Doctor slot", with: @doctor_slot.doctor_slot_id
    fill_in "Slot", with: @doctor_slot.slot_id
    fill_in "Tenant", with: @doctor_slot.tenant_id
    click_on "Update Doctor slot"

    assert_text "Doctor slot was successfully updated"
    click_on "Back"
  end

  test "destroying a Doctor slot" do
    visit doctor_slots_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Doctor slot was successfully destroyed"
  end
end
