require "application_system_test_case"

class PrescriptionMastersTest < ApplicationSystemTestCase
  setup do
    @prescription_master = prescription_masters(:one)
  end

  test "visiting the index" do
    visit prescription_masters_url
    assert_selector "h1", text: "Prescription Masters"
  end

  test "creating a Prescription master" do
    visit prescription_masters_url
    click_on "New Prescription Master"

    fill_in "Appointment", with: @prescription_master.appointment_id
    fill_in "Comment", with: @prescription_master.comment
    fill_in "Prescription", with: @prescription_master.prescription_id
    fill_in "Tenant", with: @prescription_master.tenant_id
    click_on "Create Prescription master"

    assert_text "Prescription master was successfully created"
    click_on "Back"
  end

  test "updating a Prescription master" do
    visit prescription_masters_url
    click_on "Edit", match: :first

    fill_in "Appointment", with: @prescription_master.appointment_id
    fill_in "Comment", with: @prescription_master.comment
    fill_in "Prescription", with: @prescription_master.prescription_id
    fill_in "Tenant", with: @prescription_master.tenant_id
    click_on "Update Prescription master"

    assert_text "Prescription master was successfully updated"
    click_on "Back"
  end

  test "destroying a Prescription master" do
    visit prescription_masters_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Prescription master was successfully destroyed"
  end
end
