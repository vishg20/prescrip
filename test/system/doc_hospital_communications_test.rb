require "application_system_test_case"

class DocHospitalCommunicationsTest < ApplicationSystemTestCase
  setup do
    @doc_hospital_communication = doc_hospital_communications(:one)
  end

  test "visiting the index" do
    visit doc_hospital_communications_url
    assert_selector "h1", text: "Doc Hospital Communications"
  end

  test "creating a Doc hospital communication" do
    visit doc_hospital_communications_url
    click_on "New Doc Hospital Communication"

    fill_in "Communication type", with: @doc_hospital_communication.communication_type
    fill_in "Customer", with: @doc_hospital_communication.customer_id
    fill_in "Email", with: @doc_hospital_communication.email_id
    fill_in "Phone number", with: @doc_hospital_communication.phone_number
    fill_in "Tenant", with: @doc_hospital_communication.tenant_id
    click_on "Create Doc hospital communication"

    assert_text "Doc hospital communication was successfully created"
    click_on "Back"
  end

  test "updating a Doc hospital communication" do
    visit doc_hospital_communications_url
    click_on "Edit", match: :first

    fill_in "Communication type", with: @doc_hospital_communication.communication_type
    fill_in "Customer", with: @doc_hospital_communication.customer_id
    fill_in "Email", with: @doc_hospital_communication.email_id
    fill_in "Phone number", with: @doc_hospital_communication.phone_number
    fill_in "Tenant", with: @doc_hospital_communication.tenant_id
    click_on "Update Doc hospital communication"

    assert_text "Doc hospital communication was successfully updated"
    click_on "Back"
  end

  test "destroying a Doc hospital communication" do
    visit doc_hospital_communications_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Doc hospital communication was successfully destroyed"
  end
end
