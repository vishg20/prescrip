require "application_system_test_case"

class DoctorHolidaysTest < ApplicationSystemTestCase
  setup do
    @doctor_holiday = doctor_holidays(:one)
  end

  test "visiting the index" do
    visit doctor_holidays_url
    assert_selector "h1", text: "Doctor Holidays"
  end

  test "creating a Doctor holiday" do
    visit doctor_holidays_url
    click_on "New Doctor Holiday"

    fill_in "Affiliation", with: @doctor_holiday.affiliation_id
    fill_in "Date", with: @doctor_holiday.date
    fill_in "Doctor holiday", with: @doctor_holiday.doctor_holiday_id
    fill_in "Doctor slot", with: @doctor_holiday.doctor_slot_id
    fill_in "Slot", with: @doctor_holiday.slot_id
    fill_in "Tenant", with: @doctor_holiday.tenant_id
    click_on "Create Doctor holiday"

    assert_text "Doctor holiday was successfully created"
    click_on "Back"
  end

  test "updating a Doctor holiday" do
    visit doctor_holidays_url
    click_on "Edit", match: :first

    fill_in "Affiliation", with: @doctor_holiday.affiliation_id
    fill_in "Date", with: @doctor_holiday.date
    fill_in "Doctor holiday", with: @doctor_holiday.doctor_holiday_id
    fill_in "Doctor slot", with: @doctor_holiday.doctor_slot_id
    fill_in "Slot", with: @doctor_holiday.slot_id
    fill_in "Tenant", with: @doctor_holiday.tenant_id
    click_on "Update Doctor holiday"

    assert_text "Doctor holiday was successfully updated"
    click_on "Back"
  end

  test "destroying a Doctor holiday" do
    visit doctor_holidays_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Doctor holiday was successfully destroyed"
  end
end
