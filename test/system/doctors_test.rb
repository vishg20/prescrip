require "application_system_test_case"

class DoctorsTest < ApplicationSystemTestCase
  setup do
    @doctor = doctors(:one)
  end

  test "visiting the index" do
    visit doctors_url
    assert_selector "h1", text: "Doctors"
  end

  test "creating a Doctor" do
    visit doctors_url
    click_on "New Doctor"

    fill_in "Age", with: @doctor.age
    fill_in "Customer", with: @doctor.customer_id
    fill_in "Data source", with: @doctor.data_source
    fill_in "Degree", with: @doctor.degree
    fill_in "Experience", with: @doctor.experience
    fill_in "Fees", with: @doctor.fees
    fill_in "First name", with: @doctor.first_name
    fill_in "Last name", with: @doctor.last_name
    fill_in "Middle name", with: @doctor.middle_name
    fill_in "Opt out", with: @doctor.opt_out
    fill_in "Rating", with: @doctor.rating
    fill_in "Sex", with: @doctor.sex
    fill_in "Speciality", with: @doctor.speciality
    fill_in "Status", with: @doctor.status
    fill_in "Tenant", with: @doctor.tenant_id
    fill_in "Web address", with: @doctor.web_address
    click_on "Create Doctor"

    assert_text "Doctor was successfully created"
    click_on "Back"
  end

  test "updating a Doctor" do
    visit doctors_url
    click_on "Edit", match: :first

    fill_in "Age", with: @doctor.age
    fill_in "Customer", with: @doctor.customer_id
    fill_in "Data source", with: @doctor.data_source
    fill_in "Degree", with: @doctor.degree
    fill_in "Experience", with: @doctor.experience
    fill_in "Fees", with: @doctor.fees
    fill_in "First name", with: @doctor.first_name
    fill_in "Last name", with: @doctor.last_name
    fill_in "Middle name", with: @doctor.middle_name
    fill_in "Opt out", with: @doctor.opt_out
    fill_in "Rating", with: @doctor.rating
    fill_in "Sex", with: @doctor.sex
    fill_in "Speciality", with: @doctor.speciality
    fill_in "Status", with: @doctor.status
    fill_in "Tenant", with: @doctor.tenant_id
    fill_in "Web address", with: @doctor.web_address
    click_on "Update Doctor"

    assert_text "Doctor was successfully updated"
    click_on "Back"
  end

  test "destroying a Doctor" do
    visit doctors_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Doctor was successfully destroyed"
  end
end
