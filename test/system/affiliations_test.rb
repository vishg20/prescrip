require "application_system_test_case"

class AffiliationsTest < ApplicationSystemTestCase
  setup do
    @affiliation = affiliations(:one)
  end

  test "visiting the index" do
    visit affiliations_url
    assert_selector "h1", text: "Affiliations"
  end

  test "creating a Affiliation" do
    visit affiliations_url
    click_on "New Affiliation"

    fill_in "Address", with: @affiliation.address_id
    fill_in "Affiliation", with: @affiliation.affiliation_id
    fill_in "Afiliation type", with: @affiliation.afiliation_type
    fill_in "Doctor", with: @affiliation.doctor_id
    fill_in "From customer", with: @affiliation.from_customer_id
    fill_in "Hospital", with: @affiliation.hospital_id
    fill_in "Status", with: @affiliation.status
    fill_in "Tenant", with: @affiliation.tenant_id
    fill_in "To customer", with: @affiliation.to_customer_id
    click_on "Create Affiliation"

    assert_text "Affiliation was successfully created"
    click_on "Back"
  end

  test "updating a Affiliation" do
    visit affiliations_url
    click_on "Edit", match: :first

    fill_in "Address", with: @affiliation.address_id
    fill_in "Affiliation", with: @affiliation.affiliation_id
    fill_in "Afiliation type", with: @affiliation.afiliation_type
    fill_in "Doctor", with: @affiliation.doctor_id
    fill_in "From customer", with: @affiliation.from_customer_id
    fill_in "Hospital", with: @affiliation.hospital_id
    fill_in "Status", with: @affiliation.status
    fill_in "Tenant", with: @affiliation.tenant_id
    fill_in "To customer", with: @affiliation.to_customer_id
    click_on "Update Affiliation"

    assert_text "Affiliation was successfully updated"
    click_on "Back"
  end

  test "destroying a Affiliation" do
    visit affiliations_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Affiliation was successfully destroyed"
  end
end
