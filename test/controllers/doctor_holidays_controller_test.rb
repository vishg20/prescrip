require 'test_helper'

class DoctorHolidaysControllerTest < ActionDispatch::IntegrationTest
  setup do
    @doctor_holiday = doctor_holidays(:one)
  end

  test "should get index" do
    get doctor_holidays_url
    assert_response :success
  end

  test "should get new" do
    get new_doctor_holiday_url
    assert_response :success
  end

  test "should create doctor_holiday" do
    assert_difference('DoctorHoliday.count') do
      post doctor_holidays_url, params: { doctor_holiday: { affiliation_id: @doctor_holiday.affiliation_id, date: @doctor_holiday.date, doctor_holiday_id: @doctor_holiday.doctor_holiday_id, tenant_id: @doctor_holiday.tenant_id } }
    end

    assert_redirected_to doctor_holiday_url(DoctorHoliday.last)
  end

  test "should show doctor_holiday" do
    get doctor_holiday_url(@doctor_holiday)
    assert_response :success
  end

  test "should get edit" do
    get edit_doctor_holiday_url(@doctor_holiday)
    assert_response :success
  end

  test "should update doctor_holiday" do
    patch doctor_holiday_url(@doctor_holiday), params: { doctor_holiday: { affiliation_id: @doctor_holiday.affiliation_id, date: @doctor_holiday.date, doctor_holiday_id: @doctor_holiday.doctor_holiday_id, doctor_slot_id: @doctor_holiday.doctor_slot_id, slot_id: @doctor_holiday.slot_id, tenant_id: @doctor_holiday.tenant_id } }
    assert_redirected_to doctor_holiday_url(@doctor_holiday)
  end

  test "should destroy doctor_holiday" do
    assert_difference('DoctorHoliday.count', -1) do
      delete doctor_holiday_url(@doctor_holiday)
    end

    assert_redirected_to doctor_holidays_url
  end
end
