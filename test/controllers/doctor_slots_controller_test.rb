require 'test_helper'

class DoctorSlotsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @doctor_slot = doctor_slots(:one)
  end

  test "should get index" do
    get doctor_slots_url
    assert_response :success
  end

  test "should get new" do
    get new_doctor_slot_url
    assert_response :success
  end

  test "should create doctor_slot" do
    assert_difference('DoctorSlot.count') do
      post doctor_slots_url, params: { doctor_slot: { affiliation_id: @doctor_slot.affiliation_id, day_id: @doctor_slot.day_id, doctor_slot_id: @doctor_slot.doctor_slot_id, slot_id: @doctor_slot.slot_id, tenant_id: @doctor_slot.tenant_id } }
    end

    assert_redirected_to doctor_slot_url(DoctorSlot.last)
  end

  test "should show doctor_slot" do
    get doctor_slot_url(@doctor_slot)
    assert_response :success
  end

  test "should get edit" do
    get edit_doctor_slot_url(@doctor_slot)
    assert_response :success
  end

  test "should update doctor_slot" do
    patch doctor_slot_url(@doctor_slot), params: { doctor_slot: { affiliation_id: @doctor_slot.affiliation_id, day_id: @doctor_slot.day_id, doctor_slot_id: @doctor_slot.doctor_slot_id, slot_id: @doctor_slot.slot_id, tenant_id: @doctor_slot.tenant_id } }
    assert_redirected_to doctor_slot_url(@doctor_slot)
  end

  test "should destroy doctor_slot" do
    assert_difference('DoctorSlot.count', -1) do
      delete doctor_slot_url(@doctor_slot)
    end

    assert_redirected_to doctor_slots_url
  end
end
