require 'test_helper'

class DocHospitalCommunicationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @doc_hospital_communication = doc_hospital_communications(:one)
  end

  test "should get index" do
    get doc_hospital_communications_url
    assert_response :success
  end

  test "should get new" do
    get new_doc_hospital_communication_url
    assert_response :success
  end

  test "should create doc_hospital_communication" do
    assert_difference('DocHospitalCommunication.count') do
      post doc_hospital_communications_url, params: { doc_hospital_communication: { communication_type: @doc_hospital_communication.communication_type, customer_id: @doc_hospital_communication.customer_id, email_id: @doc_hospital_communication.email_id, phone_number: @doc_hospital_communication.phone_number, tenant_id: @doc_hospital_communication.tenant_id } }
    end

    assert_redirected_to doc_hospital_communication_url(DocHospitalCommunication.last)
  end

  test "should show doc_hospital_communication" do
    get doc_hospital_communication_url(@doc_hospital_communication)
    assert_response :success
  end

  test "should get edit" do
    get edit_doc_hospital_communication_url(@doc_hospital_communication)
    assert_response :success
  end

  test "should update doc_hospital_communication" do
    patch doc_hospital_communication_url(@doc_hospital_communication), params: { doc_hospital_communication: { communication_type: @doc_hospital_communication.communication_type, customer_id: @doc_hospital_communication.customer_id, email_id: @doc_hospital_communication.email_id, phone_number: @doc_hospital_communication.phone_number, tenant_id: @doc_hospital_communication.tenant_id } }
    assert_redirected_to doc_hospital_communication_url(@doc_hospital_communication)
  end

  test "should destroy doc_hospital_communication" do
    assert_difference('DocHospitalCommunication.count', -1) do
      delete doc_hospital_communication_url(@doc_hospital_communication)
    end

    assert_redirected_to doc_hospital_communications_url
  end
end
