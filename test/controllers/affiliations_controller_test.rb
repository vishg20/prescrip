require 'test_helper'

class AffiliationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @affiliation = affiliations(:one)
  end

  test "should get index" do
    get affiliations_url
    assert_response :success
  end

  test "should get new" do
    get new_affiliation_url
    assert_response :success
  end

  test "should create affiliation" do
    assert_difference('Affiliation.count') do
      post affiliations_url, params: { affiliation: { address_id: @affiliation.address_id, affiliation_id: @affiliation.affiliation_id, afiliation_type: @affiliation.afiliation_type, doctor_id: @affiliation.doctor_id, from_customer_id: @affiliation.from_customer_id, hospital_id: @affiliation.hospital_id, status: @affiliation.status, tenant_id: @affiliation.tenant_id, to_customer_id: @affiliation.to_customer_id } }
    end

    assert_redirected_to affiliation_url(Affiliation.last)
  end

  test "should show affiliation" do
    get affiliation_url(@affiliation)
    assert_response :success
  end

  test "should get edit" do
    get edit_affiliation_url(@affiliation)
    assert_response :success
  end

  test "should update affiliation" do
    patch affiliation_url(@affiliation), params: { affiliation: { address_id: @affiliation.address_id, affiliation_id: @affiliation.affiliation_id, afiliation_type: @affiliation.afiliation_type, doctor_id: @affiliation.doctor_id, from_customer_id: @affiliation.from_customer_id, hospital_id: @affiliation.hospital_id, status: @affiliation.status, tenant_id: @affiliation.tenant_id, to_customer_id: @affiliation.to_customer_id } }
    assert_redirected_to affiliation_url(@affiliation)
  end

  test "should destroy affiliation" do
    assert_difference('Affiliation.count', -1) do
      delete affiliation_url(@affiliation)
    end

    assert_redirected_to affiliations_url
  end
end
