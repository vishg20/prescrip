require 'test_helper'

class PrescriptionMastersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @prescription_master = prescription_masters(:one)
  end

  test "should get index" do
    get prescription_masters_url
    assert_response :success
  end

  test "should get new" do
    get new_prescription_master_url
    assert_response :success
  end

  test "should create prescription_master" do
    assert_difference('PrescriptionMaster.count') do
      post prescription_masters_url, params: { prescription_master: { appointment_id: @prescription_master.appointment_id, comment: @prescription_master.comment, prescription_id: @prescription_master.prescription_id, tenant_id: @prescription_master.tenant_id } }
    end

    assert_redirected_to prescription_master_url(PrescriptionMaster.last)
  end

  test "should show prescription_master" do
    get prescription_master_url(@prescription_master)
    assert_response :success
  end

  test "should get edit" do
    get edit_prescription_master_url(@prescription_master)
    assert_response :success
  end

  test "should update prescription_master" do
    patch prescription_master_url(@prescription_master), params: { prescription_master: { appointment_id: @prescription_master.appointment_id, comment: @prescription_master.comment, prescription_id: @prescription_master.prescription_id, tenant_id: @prescription_master.tenant_id } }
    assert_redirected_to prescription_master_url(@prescription_master)
  end

  test "should destroy prescription_master" do
    assert_difference('PrescriptionMaster.count', -1) do
      delete prescription_master_url(@prescription_master)
    end

    assert_redirected_to prescription_masters_url
  end
end
