class PrescriptionMastersController < ApplicationController
  before_action :set_prescription_master, only: [:show, :edit, :update, :destroy]

  # GET /prescription_masters
  # GET /prescription_masters.json
  def index
    @prescription_masters = PrescriptionMaster.all
  end

  # GET /prescription_masters/1
  # GET /prescription_masters/1.json
  def show
  end

  # GET /prescription_masters/new
  def new
    @prescription_master = PrescriptionMaster.new
  end

  # GET /prescription_masters/1/edit
  def edit
  end

  # POST /prescription_masters
  # POST /prescription_masters.json
  def create
    @prescription_master = PrescriptionMaster.new(prescription_master_params)

    respond_to do |format|
      if @prescription_master.save
        format.html { redirect_to @prescription_master, notice: 'Prescription master was successfully created.' }
        format.json { render :show, status: :created, location: @prescription_master }
      else
        format.html { render :new }
        format.json { render json: @prescription_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /prescription_masters/1
  # PATCH/PUT /prescription_masters/1.json
  def update
    respond_to do |format|
      if @prescription_master.update(prescription_master_params)
        format.html { redirect_to @prescription_master, notice: 'Prescription master was successfully updated.' }
        format.json { render :show, status: :ok, location: @prescription_master }
      else
        format.html { render :edit }
        format.json { render json: @prescription_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /prescription_masters/1
  # DELETE /prescription_masters/1.json
  def destroy
    @prescription_master.destroy
    respond_to do |format|
      format.html { redirect_to prescription_masters_url, notice: 'Prescription master was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_prescription_master
      @prescription_master = PrescriptionMaster.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def prescription_master_params
      params.require(:prescription_master).permit(:prescription_id, :appointment_id, :comment, :tenant_id, :appointment_id)
    end
end
