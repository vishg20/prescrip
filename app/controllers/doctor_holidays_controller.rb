class DoctorHolidaysController < ApplicationController
  before_action :set_doctor_holiday, only: [:show, :edit, :update, :destroy]

  # GET /doctor_holidays
  # GET /doctor_holidays.json
  def index
    @doctor_holidays = DoctorHoliday.all
  end

  # GET /doctor_holidays/1
  # GET /doctor_holidays/1.json
  def show
  end

  # GET /doctor_holidays/new
  def new
    @doctor_holiday = DoctorHoliday.new
  end

  # GET /doctor_holidays/1/edit
  def edit
  end

  # POST /doctor_holidays
  # POST /doctor_holidays.json
  def create
    @doctor_holiday = DoctorHoliday.new(doctor_holiday_params)

    respond_to do |format|
      if @doctor_holiday.save
        format.html { redirect_to @doctor_holiday, notice: 'Doctor holiday was successfully created.' }
        format.json { render :show, status: :created, location: @doctor_holiday }
      else
        format.html { render :new }
        format.json { render json: @doctor_holiday.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /doctor_holidays/1
  # PATCH/PUT /doctor_holidays/1.json
  def update
    respond_to do |format|
      if @doctor_holiday.update(doctor_holiday_params)
        format.html { redirect_to @doctor_holiday, notice: 'Doctor holiday was successfully updated.' }
        format.json { render :show, status: :ok, location: @doctor_holiday }
      else
        format.html { render :edit }
        format.json { render json: @doctor_holiday.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /doctor_holidays/1
  # DELETE /doctor_holidays/1.json
  def destroy
    @doctor_holiday.destroy
    respond_to do |format|
      format.html { redirect_to doctor_holidays_url, notice: 'Doctor holiday was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_doctor_holiday
      @doctor_holiday = DoctorHoliday.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def doctor_holiday_params
      params.require(:doctor_holiday).permit(:doctor_holiday_id, :affiliation_id, :date, :tenant_id,:affiliation_id)
    end
end
