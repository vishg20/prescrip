class DocHospitalCommunicationsController < ApplicationController
  before_action :set_doc_hospital_communication, only: [:show, :edit, :update, :destroy]

  # GET /doc_hospital_communications
  # GET /doc_hospital_communications.json
  def index
    @doc_hospital_communications = DocHospitalCommunication.all
  end

  # GET /doc_hospital_communications/1
  # GET /doc_hospital_communications/1.json
  def show
  end

  # GET /doc_hospital_communications/new
  def new
    @doc_hospital_communication = DocHospitalCommunication.new
  end

  # GET /doc_hospital_communications/1/edit
  def edit
  end

  # POST /doc_hospital_communications
  # POST /doc_hospital_communications.json
  def create
    @doc_hospital_communication = DocHospitalCommunication.new(doc_hospital_communication_params)

    respond_to do |format|
      if @doc_hospital_communication.save
        format.html { redirect_to @doc_hospital_communication, notice: 'Doc hospital communication was successfully created.' }
        format.json { render :show, status: :created, location: @doc_hospital_communication }
      else
        format.html { render :new }
        format.json { render json: @doc_hospital_communication.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /doc_hospital_communications/1
  # PATCH/PUT /doc_hospital_communications/1.json
  def update
    respond_to do |format|
      if @doc_hospital_communication.update(doc_hospital_communication_params)
        format.html { redirect_to @doc_hospital_communication, notice: 'Doc hospital communication was successfully updated.' }
        format.json { render :show, status: :ok, location: @doc_hospital_communication }
      else
        format.html { render :edit }
        format.json { render json: @doc_hospital_communication.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /doc_hospital_communications/1
  # DELETE /doc_hospital_communications/1.json
  def destroy
    @doc_hospital_communication.destroy
    respond_to do |format|
      format.html { redirect_to doc_hospital_communications_url, notice: 'Doc hospital communication was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_doc_hospital_communication
      @doc_hospital_communication = DocHospitalCommunication.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def doc_hospital_communication_params
      params.require(:doc_hospital_communication).permit(:communication_type, :phone_number, :email_id, :customer_id, :tenant_id)
    end
end
