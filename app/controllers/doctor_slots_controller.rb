class DoctorSlotsController < ApplicationController
  before_action :set_doctor_slot, only: [:show, :edit, :update, :destroy]

  # GET /doctor_slots
  # GET /doctor_slots.json
  def index
    @doctor_slots = DoctorSlot.all
  end

  # GET /doctor_slots/1
  # GET /doctor_slots/1.json
  def show
  end

  # GET /doctor_slots/new
  def new
    @doctor_slot = DoctorSlot.new
  end

  # GET /doctor_slots/1/edit
  def edit
  end

  # POST /doctor_slots
  # POST /doctor_slots.json
  def create
    @doctor_slot = DoctorSlot.new(doctor_slot_params)

    respond_to do |format|
      if @doctor_slot.save
        format.html { redirect_to @doctor_slot, notice: 'Doctor slot was successfully created.' }
        format.json { render :show, status: :created, location: @doctor_slot }
      else
        format.html { render :new }
        format.json { render json: @doctor_slot.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /doctor_slots/1
  # PATCH/PUT /doctor_slots/1.json
  def update
    respond_to do |format|
      if @doctor_slot.update(doctor_slot_params)
        format.html { redirect_to @doctor_slot, notice: 'Doctor slot was successfully updated.' }
        format.json { render :show, status: :ok, location: @doctor_slot }
      else
        format.html { render :edit }
        format.json { render json: @doctor_slot.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /doctor_slots/1
  # DELETE /doctor_slots/1.json
  def destroy
    @doctor_slot.destroy
    respond_to do |format|
      format.html { redirect_to doctor_slots_url, notice: 'Doctor slot was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_doctor_slot
      @doctor_slot = DoctorSlot.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def doctor_slot_params
      params.require(:doctor_slot).permit(:doctor_slot_id, :affiliation_id, :day_id, :slot_id, :tenant_id, :affiliation_id)
    end
end
