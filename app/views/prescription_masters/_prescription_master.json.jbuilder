json.extract! prescription_master, :id, :prescription_id, :appointment_id, :comment, :tenant_id, :appointment_id, :created_at, :updated_at
json.url prescription_master_url(prescription_master, format: :json)
