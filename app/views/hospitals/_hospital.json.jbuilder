json.extract! hospital, :id, :customer_id, :name, :speciality, :Status, :No_of_Beds, :tenant_id, :created_at, :updated_at
json.url hospital_url(hospital, format: :json)
