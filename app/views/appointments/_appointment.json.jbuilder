json.extract! appointment, :id, :appointment_id, :customer_id, :patient_id, :name_of_the_patient, :comment, :date, :doctor_slot_id, :locked, :fees_paid, :tenant_id, :patient_id, :doctor_id, :doctor_slot_id, :created_at, :updated_at
json.url appointment_url(appointment, format: :json)
