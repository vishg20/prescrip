json.extract! patient, :id, :patient_id, :first_name, :middle_name, :last_name, :age, :sex, :date_of_birth, :status, :address, :city, :state, :country, :tenant_id, :created_at, :updated_at
json.url patient_url(patient, format: :json)
