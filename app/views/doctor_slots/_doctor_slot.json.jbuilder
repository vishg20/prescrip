json.extract! doctor_slot, :id, :doctor_slot_id, :affiliation_id, :day_id, :slot_id, :tenant_id, :affiliation_id, :created_at, :updated_at
json.url doctor_slot_url(doctor_slot, format: :json)
