json.extract! doctor, :id, :customer_id, :first_name, :middle_name, :last_name, :age, :sex, :speciality, :rating, :degree, :experience, :web_address, :opt_out, :status, :fees, :tenant_id, :created_at, :updated_at
json.url doctor_url(doctor, format: :json)
