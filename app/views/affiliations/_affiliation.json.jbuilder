json.extract! affiliation, :id, :affiliation_id, :from_customer_id, :to_customer_id, :afiliation_type, :address_id, :status, :tenant_id, :doctor_id, :hospital_id, :address_id, :created_at, :updated_at
json.url affiliation_url(affiliation, format: :json)
