json.extract! doc_hospital_communication, :id, :communication_type, :phone_number, :email_id, :customer_id, :tenant_id, :created_at, :updated_at
json.url doc_hospital_communication_url(doc_hospital_communication, format: :json)
