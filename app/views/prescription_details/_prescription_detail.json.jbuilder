json.extract! prescription_detail, :id, :prescription_detail_id, :prescription_id, :drug_name, :morning, :quantity, :afternoon, :quantity, :night, :quantity, :before_food, :after_food, :comment, :tenant_id, :prescription_master_id, :created_at, :updated_at
json.url prescription_detail_url(prescription_detail, format: :json)
