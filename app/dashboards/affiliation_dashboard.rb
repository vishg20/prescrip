require "administrate/base_dashboard"

class AffiliationDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    doctor: Field::BelongsTo,
    hospital: Field::BelongsTo,
    address: Field::BelongsTo,
    id: Field::Number,
    affiliation_id: Field::Number,
    from_customer_id: Field::Number,
    to_customer_id: Field::Number,
    afiliation_type: Field::String,
    status: Field::String,
    tenant_id: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :doctor,
    :hospital,
    :address,
    :id,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :doctor,
    :hospital,
    :address,
    :id,
    :affiliation_id,
    :from_customer_id,
    :to_customer_id,
    :afiliation_type,
    :status,
    :tenant_id,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :doctor,
    :hospital,
    :address,
    :affiliation_id,
    :from_customer_id,
    :to_customer_id,
    :afiliation_type,
    :status,
    :tenant_id,
  ].freeze

  # Overwrite this method to customize how affiliations are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(affiliation)
  #   "Affiliation ##{affiliation.id}"
  # end
end
