require "administrate/base_dashboard"

class PrescriptionMasterDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    appointment: Field::BelongsTo,
    id: Field::Number,
    prescription_id: Field::Number,
    comment: Field::Text,
    tenant_id: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :appointment,
    :id,
    :prescription_id,
    :comment,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :appointment,
    :id,
    :prescription_id,
    :comment,
    :tenant_id,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :appointment,
    :prescription_id,
    :comment,
    :tenant_id,
  ].freeze

  # Overwrite this method to customize how prescription masters are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(prescription_master)
  #   "PrescriptionMaster ##{prescription_master.id}"
  # end
end
