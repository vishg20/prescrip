require "administrate/base_dashboard"

class DoctorHolidayDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    affiliation: Field::BelongsTo,
    id: Field::Number,
    doctor_holiday_id: Field::Number,
    date: Field::DateTime,
    tenant_id: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    doctor_slot_id: Field::Number,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :affiliation,
    :id,
    :doctor_holiday_id,
    :date,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :affiliation,
    :id,
    :doctor_holiday_id,
    :date,
    :tenant_id,
    :created_at,
    :updated_at,
    :doctor_slot_id,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :affiliation,
    :doctor_holiday_id,
    :date,
    :tenant_id,
    :doctor_slot_id,
  ].freeze

  # Overwrite this method to customize how doctor holidays are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(doctor_holiday)
  #   "DoctorHoliday ##{doctor_holiday.id}"
  # end
end
