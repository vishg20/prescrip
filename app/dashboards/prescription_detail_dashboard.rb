require "administrate/base_dashboard"

class PrescriptionDetailDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    prescription_master: Field::BelongsTo,
    id: Field::Number,
    prescription_detail_id: Field::Number,
    prescription_id: Field::Number,
    drug_name: Field::String,
    morning: Field::Boolean,
    quantity: Field::String,
    afternoon: Field::Boolean,
    night: Field::Boolean,
    before_food: Field::Boolean,
    after_food: Field::Boolean,
    comment: Field::Text,
    tenant_id: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :prescription_master,
    :id,
    :prescription_detail_id,
    :prescription_id,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :prescription_master,
    :id,
    :prescription_detail_id,
    :prescription_id,
    :drug_name,
    :morning,
    :quantity,
    :afternoon,
    :night,
    :before_food,
    :after_food,
    :comment,
    :tenant_id,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :prescription_master,
    :prescription_detail_id,
    :prescription_id,
    :drug_name,
    :morning,
    :quantity,
    :afternoon,
    :night,
    :before_food,
    :after_food,
    :comment,
    :tenant_id,
  ].freeze

  # Overwrite this method to customize how prescription details are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(prescription_detail)
  #   "PrescriptionDetail ##{prescription_detail.id}"
  # end
end
