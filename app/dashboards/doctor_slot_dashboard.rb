require "administrate/base_dashboard"

class DoctorSlotDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    affiliations: Field::HasMany,
    id: Field::Number,
    doctor_slot_id: Field::Number,
    affiliation_id: Field::Number,
    day_id: Field::Number,
    slot_id: Field::Number,
    tenant_id: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :affiliations,
    :id,
    :doctor_slot_id,
    :affiliation_id,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :affiliations,
    :id,
    :doctor_slot_id,
    :affiliation_id,
    :day_id,
    :slot_id,
    :tenant_id,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :affiliations,
    :doctor_slot_id,
    :affiliation_id,
    :day_id,
    :slot_id,
    :tenant_id,
  ].freeze

  # Overwrite this method to customize how doctor slots are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(doctor_slot)
  #   "DoctorSlot ##{doctor_slot.id}"
  # end
end
