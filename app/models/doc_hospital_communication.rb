class DocHospitalCommunication < ApplicationRecord
  


	#validates
#validates :appointment_id, presence: true
validates :customer_id, presence: true
validates :phone_number, presence: true
validates :email_id, presence: true

end

# == Schema Information
#
# Table name: doc_hospital_communications
#
#  id                 :bigint(8)        not null, primary key
#  communication_type :string(255)
#  phone_number       :string(255)
#  email_id           :string(255)
#  customer_id        :integer
#  tenant_id          :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
