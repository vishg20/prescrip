class DoctorSlot < ApplicationRecord
  has_and_belongs_to_many :affiliations

  
 



  # validations
  validates :doctor_slot_id, presence: true
  validates :affiliation_id, presence: true
  validates :slot_id, presence: true  
end

# == Schema Information
#
# Table name: doctor_slots
#
#  id             :bigint(8)        not null, primary key
#  doctor_slot_id :integer
#  affiliation_id :bigint(8)
#  day_id         :integer
#  slot_id        :integer
#  tenant_id      :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
