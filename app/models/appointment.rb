class Appointment < ApplicationRecord
  belongs_to :patient
  belongs_to :doctor
  belongs_to :doctor_slot

  # validates
  validates :appointment_id, presence: true
  validates :customer_id, presence: true
  validates :patient_id, presence: true
  validates :doctor_slot_id, presence: true 
end

# == Schema Information
#
# Table name: appointments
#
#  id                  :bigint(8)        not null, primary key
#  appointment_id      :integer
#  customer_id         :integer
#  patient_id          :bigint(8)
#  name_of_the_patient :string(255)
#  comment             :text(65535)
#  date                :date
#  doctor_slot_id      :bigint(8)
#  locked              :boolean
#  fees_paid           :float(24)
#  tenant_id           :integer
#  doctor_id           :bigint(8)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#


