class Address < ApplicationRecord
  # validations
  validates :address_line_1, presence: true
  validates :address_line_2, presence: true
  validates :city, presence: true
  validates :state, presence: true
  validates :country, presence: true
end

# == Schema Information
#
# Table name: addresses
#
#  id             :bigint(8)        not null, primary key
#  address_line_1 :string(255)
#  address_line_2 :string(255)
#  city           :string(255)
#  state          :string(255)
#  country        :string(255)
#  Longitude      :float(24)
#  latitude       :float(24)
#  status         :string(255)
#  data_source    :string(255)
#  tenant_id      :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
