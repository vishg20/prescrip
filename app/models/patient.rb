class Patient < ApplicationRecord
  has_many :appointments
  has_many :doctors, through: :appointments


  # validations

  validates :patient_id, presence: true
  validates :first_name, presence: { message: 'must be given please' }
  validates :last_name, presence: { message: 'must be given please' }
  validates :age, numericality: { only_integer: true }
  validates :date_of_birth,presence: { message: 'must be given please' }
  validates :address, presence: { message: 'must be given please'}
  validates :city,presence: { message: 'must be given please' }
  validates :state,presence: { message: 'must be given please' }
  validates :country,presence: { message: 'must be given please' }
end

# == Schema Information
#
# Table name: patients
#
#  id            :bigint(8)        not null, primary key
#  patient_id    :integer
#  first_name    :string(255)
#  middle_name   :string(255)
#  last_name     :string(255)
#  age           :integer
#  sex           :string(255)
#  date_of_birth :date
#  status        :string(255)
#  address       :string(255)
#  city          :string(255)
#  state         :string(255)
#  country       :string(255)
#  data_source   :string(255)
#  tenant_id     :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
