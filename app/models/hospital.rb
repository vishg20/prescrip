class Hospital < ApplicationRecord
  has_many :doctors, through: :affiliations
  has_many :patients, through: :appointments
  




  # validations
  validates :customer_id, presence: true
  validates :name,presence: { message: 'must be given please' }
  validates :speciality,presence: { message: 'must be given please' }
  validates :No_of_Beds, numericality: { only_integer: true }

end


# == Schema Information
#
# Table name: hospitals
#
#  id          :bigint(8)        not null, primary key
#  customer_id :integer
#  name        :string(255)
#  speciality  :string(255)
#  Status      :string(255)
#  No_of_Beds  :integer
#  data_source :string(255)
#  tenant_id   :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
