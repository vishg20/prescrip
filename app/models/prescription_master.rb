class PrescriptionMaster < ApplicationRecord
  belongs_to :appointment

  # validations
  validates :prescription_id, presence: true
  validates :appointment_id, presence: true
end

# == Schema Information
#
# Table name: prescription_masters
#
#  id              :bigint(8)        not null, primary key
#  prescription_id :integer
#  appointment_id  :bigint(8)
#  comment         :text(65535)
#  tenant_id       :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#