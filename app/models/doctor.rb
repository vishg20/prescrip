class Doctor < ApplicationRecord
  has_many :appointments
  has_many :patients, through: :appointments
 

 
  #validations
 validates :customer_id, presence: true
 validates :first_name, presence:  { message: "must be given please" }
 validates :last_name, presence: true
 validates :age , numericality: { only_integer: true }
 validates :sex, presence: true 


end

# == Schema Information
#
# Table name: doctors
#
#  id          :bigint(8)        not null, primary key
#  customer_id :integer
#  first_name  :string(255)
#  middle_name :string(255)
#  last_name   :string(255)
#  age         :integer
#  sex         :string(255)
#  speciality  :string(255)
#  rating      :string(255)
#  degree      :string(255)
#  experience  :integer
#  web_address :string(255)
#  opt_out     :boolean
#  status      :string(255)
#  fees        :float(24)
#  data_source :string(255)
#  tenant_id   :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

