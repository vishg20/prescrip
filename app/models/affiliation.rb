class Affiliation < ApplicationRecord
  belongs_to :doctor
  belongs_to :hospital
  belongs_to :address
  
    # validations
  validates :from_customer_id, presence: true
  validates :to_customer_id, presence: true
  validates :address_id, presence: true
end

# == Schema Information
#
# Table name: affiliations
#
#  id               :bigint(8)        not null, primary key
#  affiliation_id   :integer
#  from_customer_id :integer
#  to_customer_id   :integer
#  afiliation_type  :string(255)
#  address_id       :bigint(8)
#  status           :string(255)
#  tenant_id        :integer
#  doctor_id        :bigint(8)
#  hospital_id      :bigint(8)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
