Rails.application.routes.draw do
  resources :data_sources
  namespace :admin do
      resources :addresses
      resources :affiliations
      resources :appointments
      resources :doc_hospital_communications
      resources :doctors
      resources :doctor_holidays
      resources :doctor_slots
      resources :hospitals
      resources :patients
      resources :prescription_details
      resources :prescription_masters

      root to: "addresses#index"
    end
  resources :doctor_holidays
  resources :prescription_details
  resources :prescription_masters
  resources :appointments
  resources :doctor_slots
  resources :patients
  resources :doc_hospital_communications
  resources :affiliations
  resources :addresses
  resources :hospitals
  resources :doctors
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
