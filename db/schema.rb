# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_06_054812) do

  create_table "addresses", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "address_line_1"
    t.string "address_line_2"
    t.string "city"
    t.string "state"
    t.string "country"
    t.float "Longitude"
    t.float "latitude"
    t.string "status"
    t.integer "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "affiliations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "affiliation_id"
    t.integer "from_customer_id"
    t.integer "to_customer_id"
    t.string "afiliation_type"
    t.bigint "address_id"
    t.string "status"
    t.integer "tenant_id"
    t.bigint "doctor_id"
    t.bigint "hospital_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["address_id"], name: "index_affiliations_on_address_id"
    t.index ["doctor_id"], name: "index_affiliations_on_doctor_id"
    t.index ["hospital_id"], name: "index_affiliations_on_hospital_id"
  end

  create_table "appointments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "appointment_id"
    t.integer "customer_id"
    t.bigint "patient_id"
    t.string "name_of_the_patient"
    t.text "comment"
    t.date "date"
    t.bigint "doctor_slot_id"
    t.boolean "locked"
    t.float "fees_paid"
    t.integer "tenant_id"
    t.bigint "doctor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["doctor_id"], name: "index_appointments_on_doctor_id"
    t.index ["doctor_slot_id"], name: "index_appointments_on_doctor_slot_id"
    t.index ["patient_id"], name: "index_appointments_on_patient_id"
  end

  create_table "data_sources", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "doc_hospital_communications", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "communication_type"
    t.string "phone_number"
    t.string "email_id"
    t.integer "customer_id"
    t.integer "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "doctor_holidays", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "doctor_holiday_id"
    t.bigint "affiliation_id"
    t.date "date"
    t.integer "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "doctor_slot_id"
    t.index ["affiliation_id"], name: "index_doctor_holidays_on_affiliation_id"
  end

  create_table "doctor_slots", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "doctor_slot_id"
    t.bigint "affiliation_id"
    t.integer "day_id"
    t.integer "slot_id"
    t.integer "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["affiliation_id"], name: "index_doctor_slots_on_affiliation_id"
  end

  create_table "doctors", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "customer_id"
    t.string "first_name"
    t.string "middle_name"
    t.string "last_name"
    t.integer "age"
    t.string "sex"
    t.string "speciality"
    t.string "rating"
    t.string "degree"
    t.integer "experience"
    t.string "web_address"
    t.boolean "opt_out"
    t.string "status"
    t.float "fees"
    t.integer "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hospitals", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "customer_id"
    t.string "name"
    t.string "speciality"
    t.string "Status"
    t.integer "No_of_Beds"
    t.integer "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "patients", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "patient_id"
    t.string "first_name"
    t.string "middle_name"
    t.string "last_name"
    t.integer "age"
    t.string "sex"
    t.date "date_of_birth"
    t.string "status"
    t.string "address"
    t.string "city"
    t.string "state"
    t.string "country"
    t.integer "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "prescription_details", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "prescription_detail_id"
    t.integer "prescription_id"
    t.string "drug_name"
    t.boolean "morning"
    t.string "quantity"
    t.boolean "afternoon"
    t.boolean "night"
    t.boolean "before_food"
    t.boolean "after_food"
    t.text "comment"
    t.integer "tenant_id"
    t.bigint "prescription_master_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["prescription_master_id"], name: "index_prescription_details_on_prescription_master_id"
  end

  create_table "prescription_masters", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "prescription_id"
    t.bigint "appointment_id"
    t.text "comment"
    t.integer "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["appointment_id"], name: "index_prescription_masters_on_appointment_id"
  end

  add_foreign_key "affiliations", "addresses"
  add_foreign_key "affiliations", "doctors"
  add_foreign_key "affiliations", "hospitals"
  add_foreign_key "appointments", "doctor_slots"
  add_foreign_key "appointments", "doctors"
  add_foreign_key "appointments", "patients"
  add_foreign_key "doctor_holidays", "affiliations"
  add_foreign_key "doctor_slots", "affiliations"
  add_foreign_key "prescription_details", "prescription_masters"
  add_foreign_key "prescription_masters", "appointments"
end
