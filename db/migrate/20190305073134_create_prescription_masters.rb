class CreatePrescriptionMasters < ActiveRecord::Migration[5.2]
  def change
    create_table :prescription_masters do |t|
      t.integer :prescription_id
      t.integer :appointment_id
      t.text :comment
      t.integer :tenant_id
      t.references :appointment, foreign_key: true

      t.timestamps
    end
  end
end
