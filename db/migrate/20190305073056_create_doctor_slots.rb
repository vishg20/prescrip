class CreateDoctorSlots < ActiveRecord::Migration[5.2]
  def change
    create_table :doctor_slots do |t|
      t.integer :doctor_slot_id
      t.integer :affiliation_id
      t.integer :day_id
      t.integer :slot_id
      t.integer :tenant_id
      t.references :affiliation, foreign_key: true

      t.timestamps
    end
  end
end
