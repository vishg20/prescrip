class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :address_line_1, limit: 255
      t.string :address_line_2, limit: 255
      t.string :city
      t.string :state
      t.string :country
      t.float :Longitude
      t.float :latitude
      t.string :status
     # t.string :data_source
      t.integer :tenant_id

      t.timestamps
    end
  end
end
