class CreatePrescriptionDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :prescription_details do |t|
      t.integer :prescription_detail_id
      t.integer :prescription_id
      t.string :drug_name
      t.boolean :morning
      t.float :quantity
      t.boolean :afternoon
      t.string :quantity
      t.boolean :night
      t.string :quantity
      t.boolean :before_food
      t.boolean :after_food
      t.text :comment
      t.integer :tenant_id
      t.references :prescription_master, foreign_key: true

      t.timestamps
    end
  end
end
