class RemoveDoctorSlotIdFromDoctorHolidays < ActiveRecord::Migration[5.2]
  def change
    remove_column :doctor_holidays, :doctor_slot_id, :integer
  end
end
