class RemoveDataSourceFromHospitals < ActiveRecord::Migration[5.2]
  def change
    remove_column :hospitals, :data_source, :string
  end
end
