class CreateDoctors < ActiveRecord::Migration[5.2]
  def change
    create_table :doctors do |t|
      t.integer :customer_id
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.integer :age
      t.string :sex
      t.string :speciality
      t.string :rating
      t.string :degree
      t.integer :experience
      t.string :web_address
      t.boolean :opt_out
      t.string :status
      t.float :fees
      #t.string :data_source
      t.integer :tenant_id

      t.timestamps
    end
  end
end
