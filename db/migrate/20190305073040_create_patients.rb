class CreatePatients < ActiveRecord::Migration[5.2]
  def change
    create_table :patients do |t|
      t.integer :patient_id
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.integer :age
      t.string :sex
      t.date :date_of_birth
      t.string :status
      t.string :address
      t.string :city
      t.string :state
      t.string :country
     # t.string :data_source
      t.integer :tenant_id

      t.timestamps
    end
  end
end
