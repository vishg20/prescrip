class CreateHospitals < ActiveRecord::Migration[5.2]
  def change
    create_table :hospitals do |t|
      t.integer :customer_id
      t.string :name
      t.string :speciality
      t.string :Status
      t.integer :No_of_Beds
      #t.string :data_source
      t.integer :tenant_id

      t.timestamps
    end
  end
end
