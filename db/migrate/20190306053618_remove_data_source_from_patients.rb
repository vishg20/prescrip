class RemoveDataSourceFromPatients < ActiveRecord::Migration[5.2]
  def change
    remove_column :patients, :data_source, :string
  end
end
