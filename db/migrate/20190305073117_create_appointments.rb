class CreateAppointments < ActiveRecord::Migration[5.2]
  def change
    create_table :appointments do |t|
      t.integer :appointment_id
      t.integer :customer_id
      t.integer :patient_id
      t.string :name_of_the_patient
      t.text :comment
      t.date :date
      t.integer :doctor_slot_id
      t.boolean :locked
      t.float :fees_paid
      t.integer :tenant_id
      t.references :patient, foreign_key: true
      t.references :doctor, foreign_key: true
      t.references :doctor_slot, foreign_key: true

      t.timestamps
    end
  end
end
