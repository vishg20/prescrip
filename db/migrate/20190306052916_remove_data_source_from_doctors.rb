class RemoveDataSourceFromDoctors < ActiveRecord::Migration[5.2]
  def change
    remove_column :doctors, :data_source, :string
  end
end
