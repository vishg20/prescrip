class CreateDataSources < ActiveRecord::Migration[5.2]
  def change
    create_table :data_sources do |t|
      t.string :name

      t.timestamps
    end
  end
end
