class CreateDoctorHolidays < ActiveRecord::Migration[5.2]
  def change
    create_table :doctor_holidays do |t|
      t.integer :doctor_holiday_id
      t.integer :affiliation_id
      t.date :date
      #t.integer :slot_id
      t.integer :tenant_id
      #t.references :doctor_slot, foreign_key: true
      t.references :affiliation, foreign_key: true

      t.timestamps
    end
  end
end
