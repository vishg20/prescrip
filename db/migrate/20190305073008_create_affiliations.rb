class CreateAffiliations < ActiveRecord::Migration[5.2]
  def change
    create_table :affiliations do |t|
      t.integer :affiliation_id
      t.integer :from_customer_id
      t.integer :to_customer_id
      t.string :afiliation_type
      t.string :address_id
      t.string :status
      t.integer :tenant_id
      t.references :doctor, foreign_key: true
      t.references :hospital, foreign_key: true
      t.references :address, foreign_key: true

      t.timestamps
    end
  end
end
