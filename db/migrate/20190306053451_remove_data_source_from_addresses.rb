class RemoveDataSourceFromAddresses < ActiveRecord::Migration[5.2]
  def change
    remove_column :addresses, :data_source, :string
  end
end
