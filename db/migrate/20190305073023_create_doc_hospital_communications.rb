class CreateDocHospitalCommunications < ActiveRecord::Migration[5.2]
  def change
    create_table :doc_hospital_communications do |t|
      t.string :communication_type
      t.string :phone_number
      t.string :email_id
      t.references :customer_id 
      t.integer :tenant_id

      t.timestamps
    end
  end
end
